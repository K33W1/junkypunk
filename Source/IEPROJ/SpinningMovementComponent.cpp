// Fill out your copyright notice in the Description page of Project Settings.

#include "SpinningMovementComponent.h"

USpinningMovementComponent::USpinningMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void USpinningMovementComponent::Inject(USceneComponent* SceneComponent)
{
	ComponentToMove = SceneComponent;
}

void USpinningMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (ComponentToMove)
	{
		float Yaw = ComponentToMove->GetRelativeRotation().Yaw;
		Yaw += RotationSpeed * DeltaTime;
		ComponentToMove->SetRelativeRotation(FRotator(0.0f, Yaw, 0.0f));
	}
}
