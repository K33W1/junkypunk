// Fill out your copyright notice in the Description page of Project Settings.

#include "Explosion.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Components/AudioComponent.h"

AExplosion::AExplosion()
{
	PrimaryActorTick.bCanEverTick = false;

	ParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystem"));
	SetRootComponent(ParticleSystem);

	ExplosionRadialForce = CreateDefaultSubobject<URadialForceComponent>(TEXT("ExplosionRadialForceComp"));
	ExplosionRadialForce->SetupAttachment(RootComponent);
	ExplosionRadialForce->Radius = 1024.0f;
	ExplosionRadialForce->ImpulseStrength = 1000000.0f;

	ExplosionAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("ExplosionAudioComp"));
	ExplosionAudio->SetupAttachment(RootComponent);
}

void AExplosion::BeginPlay()
{
	Super::BeginPlay();

	SetLifeSpan(5.0f);
	
	ExplosionRadialForce->FireImpulse();
}

