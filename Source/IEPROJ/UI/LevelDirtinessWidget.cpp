// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelDirtinessWidget.h"
#include "IEPROJ/Managers/LevelManager.h"
#include "Components/ProgressBar.h"

void ULevelDirtinessWidget::Inject(ALevelManager* NewLevelManager)
{
	LevelManager = NewLevelManager;
}

void ULevelDirtinessWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	
	CleanlinessBar->PercentDelegate.BindUFunction(this, FName("GetDirtinessPercent"));
}

float ULevelDirtinessWidget::GetDirtinessPercent() const
{
	return LevelManager->GetDirtinessPercent();
}
