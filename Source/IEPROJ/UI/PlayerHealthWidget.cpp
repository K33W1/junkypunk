// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerHealthWidget.h"
#include "PlayerHeartWidget.h"
#include "IEPROJ/Characters/PlayerCharacter.h"
#include "Components/HorizontalBox.h"

void UPlayerHealthWidget::Inject(APlayerCharacter* NewPlayerCharacter)
{
	PlayerCharacter = NewPlayerCharacter;
}

void UPlayerHealthWidget::UpdateHearts()
{
	if (!HeartWidgetClass.Get())
	{
		UE_LOG(LogTemp, Warning, TEXT("No PlayerHeartWidgetClass referenced!"));
		return;
	}

	if (PlayerCharacter)
	{
		const int MaxHealth = FMath::Max(PlayerCharacter->GetMaxHealth(), 0);
		const int Health = FMath::Max(PlayerCharacter->GetHealth(), 0);

		while (HeartWidgets.Num() < MaxHealth)
		{
			UPlayerHeartWidget* NewHeartWidget = CreateWidget<UPlayerHeartWidget>(PlayerCharacter->GetPlayerController(), HeartWidgetClass);
			HeartWidgets.Add(NewHeartWidget);
			HeartsBox->AddChildToHorizontalBox(NewHeartWidget);
		}

		for (int i = 0; i < Health; i++)
		{
			HeartWidgets[i]->Show();
		}

		for (int i = Health; i < MaxHealth; i++)
		{
			HeartWidgets[i]->Hide();
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Owning player is not PlayerCharacter"));
	}
}
