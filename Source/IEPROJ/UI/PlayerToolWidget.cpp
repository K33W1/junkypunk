// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerToolWidget.h"
#include "Blueprint/WidgetTree.h"

void UPlayerToolWidget::NativeConstruct()
{
	Super::NativeConstruct();

	SetMinimumDesiredSize(FVector2D(128.0f, 128.0f));
}

void UPlayerToolWidget::Inject(APlayerTool* NewPlayerTool)
{
	PlayerTool = NewPlayerTool;
}

void UPlayerToolWidget::OnEquip()
{
	SetMinimumDesiredSize(FVector2D(256.0f, 256.0f));
}

void UPlayerToolWidget::OnUnequip()
{
	SetMinimumDesiredSize(FVector2D(128.0f, 128.0f));
}

void UPlayerToolWidget::OnUnattach()
{
	RemoveFromParent();
	
	PlayerTool = nullptr;
}
