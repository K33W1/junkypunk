// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHealthWidget.generated.h"

class APlayerCharacter;
class UPlayerHeartWidget;
class UHorizontalBox;

UCLASS(Abstract, Blueprintable, BlueprintType)
class IEPROJ_API UPlayerHealthWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void Inject(APlayerCharacter* NewPlayerCharacter);
	
	void UpdateHearts();

private:
	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UPlayerHeartWidget> HeartWidgetClass;
	
	UPROPERTY(meta = (BindWidget, AllowPrivateAccess = "true"))
	UHorizontalBox* HeartsBox;

	APlayerCharacter* PlayerCharacter;
	
	TArray<UPlayerHeartWidget*> HeartWidgets;
};
