// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerToolWidget.generated.h"

class UCanvasPanel;
class UImage;
class APlayerTool;

UCLASS(Abstract, Blueprintable, BlueprintType)
class IEPROJ_API UPlayerToolWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void NativeConstruct() override;
	
	void Inject(APlayerTool* NewPlayerTool);
	
	void OnEquip();
	void OnUnequip();
	void OnUnattach();

protected:
	UPROPERTY(meta = (BindWidget))
	UCanvasPanel* Canvas;
	
	UPROPERTY(meta = (BindWidget))
	UImage* ToolImage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	APlayerTool* PlayerTool;
};
