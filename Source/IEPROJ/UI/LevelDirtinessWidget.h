// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LevelDirtinessWidget.generated.h"

class ALevelManager;
class UProgressBar;
class AMainGameMode;

UCLASS(Abstract, Blueprintable, BlueprintType)
class IEPROJ_API ULevelDirtinessWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void Inject(ALevelManager* NewLevelManager);
	
protected:
	virtual void NativeOnInitialized() override;

private:
	UFUNCTION(BlueprintPure)
	float GetDirtinessPercent() const;
	
	UPROPERTY(meta = (BindWidget, AllowPrivateAccess = "true"))
	UProgressBar* CleanlinessBar;

	ALevelManager* LevelManager;
};
