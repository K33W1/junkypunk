// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "IEPROJ/PlayerTool.h"

#include "PlayerToolsHotbarWidget.generated.h"

class UPlayerToolWidget;
class APlayerCharacter;
class UHorizontalBox;

UCLASS(Abstract, Blueprintable, BlueprintType)
class IEPROJ_API UPlayerToolsHotbarWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetPlayerCharacter(APlayerCharacter* NewPlayerCharacter);

private:
	void InitializePlayerToolWidget(APlayerTool* PlayerTool);

	UPROPERTY(meta = (BindWidget, AllowPrivateAccess = "true"))
	UHorizontalBox* ToolsBox;

	APlayerCharacter* PlayerCharacter;
};
