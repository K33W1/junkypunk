// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerToolsHotbarWidget.h"
#include "PlayerToolWidget.h"
#include "IEPROJ/Characters/PlayerCharacter.h"
#include "IEPROJ/PlayerTool.h"
#include "Components/HorizontalBox.h"
#include "Components/HorizontalBoxSlot.h"

void UPlayerToolsHotbarWidget::SetPlayerCharacter(APlayerCharacter* NewPlayerCharacter)
{
	PlayerCharacter = NewPlayerCharacter;

	ToolsBox->ClearChildren();

	if (PlayerCharacter)
	{
		for (APlayerTool* PlayerTool : PlayerCharacter->GetPlayerTools())
		{
			PlayerTool->Initialize();
			
			InitializePlayerToolWidget(PlayerTool);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to cast owning player of PlayerToolsHUD to PlayerCharacter"));
	}
}

void UPlayerToolsHotbarWidget::InitializePlayerToolWidget(APlayerTool* PlayerTool)
{
	UPlayerToolWidget* ToolWidget = PlayerTool->GetPlayerToolWidget();

	ToolsBox->AddChildToHorizontalBox(ToolWidget);

	UHorizontalBoxSlot* HorizontalBoxSlot = Cast<UHorizontalBoxSlot>(ToolWidget->Slot);
	HorizontalBoxSlot->SetSize(ESlateSizeRule::Fill);
	HorizontalBoxSlot->SetVerticalAlignment(VAlign_Bottom);
	HorizontalBoxSlot->SetHorizontalAlignment(HAlign_Center);
}
