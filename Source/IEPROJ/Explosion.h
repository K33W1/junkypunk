// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Explosion.generated.h"

class URadialForceComponent;

UCLASS(Abstract)
class IEPROJ_API AExplosion : public AActor
{
	GENERATED_BODY()
	
public:	
	AExplosion();

	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleInstanceOnly, meta = (AllowPrivateAccess = "true"))
	UParticleSystemComponent* ParticleSystem;
	
	UPROPERTY(VisibleInstanceOnly, meta = (AllowPrivateAccess = "true"))
	URadialForceComponent* ExplosionRadialForce;
	
	UPROPERTY(VisibleInstanceOnly, meta = (AllowPrivateAccess = "true"))
	UAudioComponent* ExplosionAudio;
};
