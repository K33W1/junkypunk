// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "CameraActorRuntimeDataAsset.generated.h"

UCLASS(BlueprintType)
class IEPROJ_API UCameraActorRuntimeDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
	TArray<ACameraActor*> Cameras;
};
