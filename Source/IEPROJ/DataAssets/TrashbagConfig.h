// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ToolConfig.h"
#include "TrashbagConfig.generated.h"

UCLASS(BlueprintType)
class IEPROJ_API UTrashbagConfig : public UToolConfig
{
	GENERATED_BODY()

public:
	virtual void Copy(UToolConfig* OtherToolConfig) override;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int MaxCapacity;
};
