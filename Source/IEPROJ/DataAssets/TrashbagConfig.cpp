// Fill out your copyright notice in the Description page of Project Settings.

#include "TrashbagConfig.h"

void UTrashbagConfig::Copy(UToolConfig* OtherToolConfig)
{
	if (UTrashbagConfig* TrashbagConfig = Cast<UTrashbagConfig>(OtherToolConfig))
	{
		MaxCapacity = TrashbagConfig->MaxCapacity;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to copy other tool!"));
	}
}
