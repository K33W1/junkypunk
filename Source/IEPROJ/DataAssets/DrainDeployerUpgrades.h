// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ToolUpgrades.h"
#include "DrainDeployerUpgrades.generated.h"

UCLASS(BlueprintType)
class IEPROJ_API UDrainDeployerUpgrades : public UToolUpgrades
{
	GENERATED_BODY()
	
};
