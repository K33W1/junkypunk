// Fill out your copyright notice in the Description page of Project Settings.

#include "DrainDeployerConfig.h"

void UDrainDeployerConfig::Copy(UToolConfig* OtherToolConfig)
{
	if (UDrainDeployerConfig* TrashbagConfig = Cast<UDrainDeployerConfig>(OtherToolConfig))
	{
		StartingDrainAmount = TrashbagConfig->StartingDrainAmount;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to copy other tool!"));
	}
}
