// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelManager.h"
#include "EnemyManager.h"
#include "IEPROJ/Characters/PlayerCharacter.h"
#include "IEPROJ/DataAssets/Runtime/ActorRuntimeDataAsset.h"
#include "IEPROJ/UI/LevelDirtinessWidget.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"

ALevelManager::ALevelManager()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ALevelManager::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (!IsPlaying)
		return;

	if (bIsWarningState)
	{
		WarningTimer -= DeltaSeconds;
		if (WarningTimer <= 0.0f)
		{
			PlayerCharacter->ForceDeath();
		}
	}
	else
	{
		WarningTimer += DeltaSeconds;
		WarningTimer = FMath::Min(WarningTimer, WarningTimerMax);
	}
}

void ALevelManager::BeginPlay()
{
	Super::BeginPlay();

	check(Coins);
	check(EnemyManager);
	check(PlayerCharacterClass.Get());
	check(GameOverMenuClass.Get());
	check(DirtinessWidgetClass.Get());
	
	PlayerController = GetWorld()->GetFirstPlayerController();
	EmptyPlayerPawn = PlayerController->GetPawn();

	check(PlayerController);
	check(EmptyPlayerPawn);
	
	InitializeGameOverMenu();
	InitializeDirtinessWidget();
	InitializeGameEndTimer();
}

void ALevelManager::InitializeGameOverMenu()
{
	check(GameOverMenuClass.Get());
	GameOverMenuWidget = CreateWidget<UUserWidget>(PlayerController, GameOverMenuClass);
}

void ALevelManager::InitializeDirtinessWidget()
{
	DirtinessWidget = CreateWidget<ULevelDirtinessWidget>(
		PlayerController,
		DirtinessWidgetClass);
	DirtinessWidget->Inject(this);
}

void ALevelManager::InitializeGameEndTimer()
{
	WarningTimer = WarningTimerMax;
}

void ALevelManager::SpawnPlayerCharacter()
{
	DestroyPlayerCharacter();
	const FTransform SpawnTransform = PlayerSpawnPoint->GetTransform();
	PlayerCharacter = Cast<APlayerCharacter>(GetWorld()->SpawnActor(
		PlayerCharacterClass,
		&SpawnTransform));
	PlayerCharacter->OnPlayerDeath.AddDynamic(this, &ALevelManager::OnPlayerDeath);
}

void ALevelManager::DestroyPlayerCharacter()
{
	if (PlayerCharacter)
	{
		PlayerCharacter->OnPlayerDeath.RemoveDynamic(this, &ALevelManager::OnPlayerDeath);
		PlayerCharacter->Destroy();
		PlayerCharacter = nullptr;
	}
}

void ALevelManager::EnterLevel()
{
	// Spawn player
	SpawnPlayerCharacter();

	// Bindings
	if (DirtyActors)
	{
		DirtyActors->OnArrayUpdated.AddDynamic(this, &ALevelManager::OnDirtyComponentsArrayChanged);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("LevelManager's DirtyActors is null!"));
	}

	// Move camera to level
	PlayerController->SetViewTargetWithBlend(PlayerCharacter, CameraBlendTime);

	// Stream in the level
	FLatentActionInfo LatentInfo;
	UGameplayStatics::LoadStreamLevel(this, LevelName, true, true, LatentInfo);
	
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(
		TimerHandle,
		this,
		&ALevelManager::StartGame,
		CameraBlendTime);
}

void ALevelManager::ExitLevel()
{
	UE_LOG(LogTemp, Warning, TEXT("Exiting level!"));
	
	// Unbind
	DirtyActors->OnArrayUpdated
		.RemoveDynamic(this, &ALevelManager::OnDirtyComponentsArrayChanged);

	// Destroy everything
	DirtyActors->DestroyAll();
	DestroyPlayerCharacter();

	// Load outside level
	FLatentActionInfo LatentInfo;
	UGameplayStatics::LoadStreamLevel(this, FName("Canyon"), true, true, LatentInfo);
}

void ALevelManager::StartGame()
{
	// Unload outside map
	FLatentActionInfo LatentInfo;
	UGameplayStatics::UnloadStreamLevel(this, FName("Canyon"), LatentInfo, true);
	
	// UI
	DirtinessWidget->AddToViewport();
	
	// Game logic
	EnemyManager->StartSpawning();
	PlayerController->Possess(PlayerCharacter);
	PlayerCharacter->StartGame();
	WarningTimer = WarningTimerMax;
	IsPlaying = true;

	OnGameStarted.Broadcast();
}

void ALevelManager::RestartGame()
{
	// UI
	GameOverMenuWidget->RemoveFromParent();
	DirtinessWidget->AddToViewport();
	
	// Game logic
	Coins->DestroyAll();
	DirtyActors->DestroyAll();
	WarningTimer = 0.0f;
	bIsWarningState = false;
	EnemyManager->StartSpawning();
	
	// Spawn new player
	SpawnPlayerCharacter();
	PlayerController->Possess(PlayerCharacter);
	PlayerCharacter->StartGame();

	IsPlaying = true;
}

void ALevelManager::StopGame()
{
	PlayerController->UnPossess();
	
	// UI
	DirtinessWidget->RemoveFromParent();
	GameOverMenuWidget->AddToViewport();

	// Game logic
	EnemyManager->StopSpawning();
	EnemyManager->KillAll();

	IsPlaying = false;
}

float ALevelManager::GetDirtinessPercent() const
{
	return FMath::Min(static_cast<float>(DirtyActors->Num()) / MaxDirtyItems, 1.0f);
}

void ALevelManager::OnPlayerDeath()
{
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(
		TimerHandle,
		this,
		&ALevelManager::StopGame,
		GameOverDelayDuration);
}

void ALevelManager::OnDirtyComponentsArrayChanged(int NewSize)
{
	if (NewSize >= MaxDirtyItems)
	{
		bIsWarningState = true;
	}
	else
	{
		bIsWarningState = false;
	}
}
