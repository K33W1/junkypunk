// Fill out your copyright notice in the Description page of Project Settings.

#include "FreeRoamManager.h"
#include "LevelManager.h"
#include "IEPROJ/Characters/ThirdPersonCharacter.h"
#include "Kismet/GameplayStatics.h"

AFreeRoamManager::AFreeRoamManager()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFreeRoamManager::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = GetWorld()->GetFirstPlayerController();
	LevelManager = Cast<ALevelManager>(UGameplayStatics::GetActorOfClass(
		GetWorld(),
		ALevelManager::StaticClass()));

	LevelManager->OnGameStarted.AddDynamic(this, &AFreeRoamManager::ExitFreeRoam);
}

void AFreeRoamManager::EnterFreeRoamFromMainMenu()
{
	EnterFreeRoam(FromMainMenuSpawnPoint->GetTransform());
}

void AFreeRoamManager::EnterFreeRoamFromArena()
{
	EnterFreeRoam(FromArenaSpawnPoint->GetTransform());
}

void AFreeRoamManager::EnterFreeRoam(const FTransform& SpawnTransform)
{
	FreeRoamCharacter = Cast<AThirdPersonCharacter>(GetWorld()->SpawnActor(
		FreeRoamCharacterClass,
		&SpawnTransform));

	PlayerController->SetViewTargetWithBlend(
		FreeRoamCharacter,
		CameraBlendTime);

	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(
		TimerHandle,
		this,
		&AFreeRoamManager::OnViewBlendCompleted,
		CameraBlendTime);
}

void AFreeRoamManager::OnViewBlendCompleted()
{
	// Unload level
	FLatentActionInfo LatentInfo;
	UGameplayStatics::UnloadStreamLevel(this, FName("Arena"), LatentInfo, true);

	// Possess free roam character
	PlayerController->Possess(FreeRoamCharacter);
	PlayerController->EnableInput(PlayerController);
	PlayerController->SetInputMode(FInputModeGameOnly());
	PlayerController->bShowMouseCursor = false;
}

void AFreeRoamManager::ExitFreeRoam()
{
	FreeRoamCharacter->Destroy();
}
