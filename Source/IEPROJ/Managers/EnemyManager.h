// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IEPROJ/Characters/NPC.h"
#include "Engine/TargetPoint.h"
#include "GameFramework/Actor.h"
#include "EnemyManager.generated.h"

UCLASS(Abstract)
class IEPROJ_API AEnemyManager : public AActor
{
	GENERATED_BODY()
	
public:	
	AEnemyManager();

	UFUNCTION(BlueprintCallable)
	void StartSpawning();

	UFUNCTION(BlueprintCallable)
	void StopSpawning();
	
	UFUNCTION(BlueprintCallable)
	void KillAll();

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

private:
	UFUNCTION()
	void SpawnEnemy();

	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	UNPCRuntimeDataAsset* EnemyContainer;
	
	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<ACharacter> EnemyToSpawn;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	int MaxActiveEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float EnemySpawnRate = 1.0f;

	// Instance only
	UPROPERTY(EditInstanceOnly, meta = (AllowPrivateAccess = "true"))
	TArray<ATargetPoint*> EnemySpawnPoints;

	UPROPERTY(EditInstanceOnly, meta = (AllowPrivateAccess = "true"))
	APatrolPath* EnemyPatrolPath;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool IsSpawning = false;
	
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float SpawnTimer = 0;
};
