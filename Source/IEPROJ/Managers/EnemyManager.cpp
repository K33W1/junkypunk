// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyManager.h"
#include "IEPROJ/DataAssets/Runtime/NPCRuntimeDataAsset.h"
#include "IEPROJ/AI/PatrolPath.h"

AEnemyManager::AEnemyManager()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AEnemyManager::BeginPlay()
{
	Super::BeginPlay();

	if (EnemySpawnPoints.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("No enemy spawn points assigned!"));
		return;
	}

	for (ATargetPoint* SpawnPoint : EnemySpawnPoints)
	{
		if (SpawnPoint == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("Null spawn point in array detected!"));
			return;
		}
	}

	if (*EnemyToSpawn == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("No provided enemy class to spawn!"));
		return;
	}
}

void AEnemyManager::Tick(float DeltaSeconds)
{
	if (!IsSpawning)
		return;
	
	SpawnTimer += DeltaSeconds;

	if (!EnemyContainer)
	{
		UE_LOG(LogTemp, Warning, TEXT("Enemy Container not assigned on EnemyManager!"));
		return;
	}
	
	if (SpawnTimer >= EnemySpawnRate && EnemyContainer->NPCs.Num() < MaxActiveEnemies)
	{
		SpawnTimer = 0;
		SpawnEnemy();
	}
}

void AEnemyManager::StartSpawning()
{
	IsSpawning = true;
	SpawnTimer = 0.0f;
}

void AEnemyManager::StopSpawning()
{
	IsSpawning = false;
}

void AEnemyManager::KillAll()
{
	TArray<ANPC*> NPCs = EnemyContainer->NPCs;
	
	for (ANPC* NPC : NPCs)
	{
		NPC->ForceDeath();
	}
}

void AEnemyManager::SpawnEnemy()
{
	const int RandomIndex = FMath::RandRange(0, EnemySpawnPoints.Num() - 1);
	const ATargetPoint* const SpawnPoint = EnemySpawnPoints[RandomIndex];

	if (EnemyToSpawn.Get())
	{
		const FActorSpawnParameters SpawnParams;
		ANPC* NPC = GetWorld()->SpawnActor<ANPC>(
			EnemyToSpawn,
			SpawnPoint->GetActorLocation(),
			SpawnPoint->GetActorRotation(),
			SpawnParams);
		NPC->SetPatrolPath(EnemyPatrolPath);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Enemy Manager has no EnemyToSpawn!"));
	}
}
