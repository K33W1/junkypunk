// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelManager.generated.h"

class ATargetPoint;
class UActorRuntimeDataAsset;
class AMainGameMode;
class UDirtyComponentRuntimeDataAsset;
class ULevelDirtinessWidget;
class UDirtyComponent;
class APlayerCharacter;
class AEnemyManager;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameStarted);

UCLASS(Abstract)
class IEPROJ_API ALevelManager : public AActor
{
	GENERATED_BODY()
	
public:	
	ALevelManager();

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	void EnterLevel();

	UFUNCTION(BlueprintCallable)
	void ExitLevel();
	
	UFUNCTION(BlueprintCallable)
	void StartGame();

	UFUNCTION(BlueprintCallable)
	void RestartGame();
	
	UFUNCTION(BlueprintCallable)
	void StopGame();

	float GetDirtinessPercent() const;

	// Delegates
	UPROPERTY(BlueprintAssignable)
	FOnGameStarted OnGameStarted;

protected:
	virtual void BeginPlay() override;

private:
	// Initializations
	void InitializeGameOverMenu();
	void InitializeDirtinessWidget();
	void InitializeGameEndTimer();
	void SpawnPlayerCharacter();
	void DestroyPlayerCharacter();

	// Listeners
	UFUNCTION()
	void OnPlayerDeath();

	UFUNCTION()
	void OnDirtyComponentsArrayChanged(int NewSize);
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UActorRuntimeDataAsset* Coins;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UActorRuntimeDataAsset* DirtyActors;
	
	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<APlayerCharacter> PlayerCharacterClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float CameraBlendTime = 1.5f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	int MaxDirtyItems = 20;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float GameOverDelayDuration = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float WarningTimerMax = 5.0f;

	// Instance only
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FName LevelName;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AEnemyManager* EnemyManager;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	ATargetPoint* PlayerSpawnPoint;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool bIsWarningState = false;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float WarningTimer = 0.0f;

	// UI
	UPROPERTY(EditDefaultsOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UUserWidget> GameOverMenuClass;

	UPROPERTY(EditDefaultsOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<ULevelDirtinessWidget> DirtinessWidgetClass;

	UPROPERTY(VisibleInstanceOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	ULevelDirtinessWidget* DirtinessWidget;
	
	UPROPERTY(VisibleInstanceOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	UUserWidget* GameOverMenuWidget;

	AMainGameMode* MainGameMode;

	// Game loop
	bool IsPlaying;
	APlayerController* PlayerController;
	APlayerCharacter* PlayerCharacter;
	APawn* EmptyPlayerPawn;
};
