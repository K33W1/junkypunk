// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AddSelfToRuntimeDataAssetComponent.generated.h"

class UActorRuntimeDataAsset;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class IEPROJ_API UAddSelfToRuntimeDataAssetComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UAddSelfToRuntimeDataAssetComponent();

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UActorRuntimeDataAsset* ActorRuntimeDataAsset;
};
