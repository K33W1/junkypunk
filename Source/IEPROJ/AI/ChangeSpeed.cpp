// Fill out your copyright notice in the Description page of Project Settings.

#include "ChangeSpeed.h"
#include "AIController.h"
#include "IEPROJ/Characters/NPC.h"
#include "GameFramework/CharacterMovementComponent.h"

UChangeSpeed::UChangeSpeed()
{
	NodeName = TEXT("Change Speed");
	bNotifyBecomeRelevant = true;
}

void UChangeSpeed::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);

	AAIController* Controller = OwnerComp.GetAIOwner();
	ANPC* NPC = Cast<ANPC>(Controller->GetCharacter());
	NPC->GetCharacterMovement()->MaxWalkSpeed = Speed;
}

FString UChangeSpeed::GetStaticServiceDescription() const
{
	return FString("Change NPC max walk speed");
}
