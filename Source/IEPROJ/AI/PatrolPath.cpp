// Fill out your copyright notice in the Description page of Project Settings.

#include "PatrolPath.h"

APatrolPath::APatrolPath()
{
	PrimaryActorTick.bCanEverTick = false;

}

FVector APatrolPath::GetPatrolPointLocation(const int Index) const
{
	const FVector LocalPatrolPoint = PatrolPoints[Index];
	return GetActorTransform().TransformPosition(LocalPatrolPoint);
}

FVector APatrolPath::GetRandomPatrolPointLocation() const
{
	int RandomPatrolPointIndex = FMath::RandRange(0, PatrolPoints.Num() - 1);
	const FVector LocalPatrolPoint = PatrolPoints[RandomPatrolPointIndex];
	return GetActorTransform().TransformPosition(LocalPatrolPoint);
}

int APatrolPath::Num() const
{
	return PatrolPoints.Num();
}
