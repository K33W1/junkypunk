#pragma once

namespace BB_Keys
{
	const TCHAR* const Target = TEXT("Target");
	const TCHAR* const SearchRadius = TEXT("SearchRadius");
	const TCHAR* const AttackRadius = TEXT("AttackRadius");
	const TCHAR* const TargetVector = TEXT("TargetVector");
	const TCHAR* const PatrolPathIndex = TEXT("PatrolPathIndex");
}