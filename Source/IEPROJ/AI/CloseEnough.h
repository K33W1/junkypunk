// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "CloseEnough.generated.h"

/**
 * 
 */
UCLASS()
class IEPROJ_API UCloseEnough : public UBTDecorator
{
	GENERATED_BODY()

public:
	UCloseEnough();

protected:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
