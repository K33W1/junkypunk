// Fill out your copyright notice in the Description page of Project Settings.


#include "CloseEnough.h"
#include "BlackboardKeys.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"

UCloseEnough::UCloseEnough()
{
	NodeName = FString("Close Enough");
}

bool UCloseEnough::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	AAIController* AIController = OwnerComp.GetAIOwner();
	
	AActor* Owner = AIController->GetPawn();
	AActor* Target = Cast<AActor>(Blackboard->GetValueAsObject(BB_Keys::Target));

	FVector OwnerLoc = Owner->GetActorLocation();
	FVector TargetLoc = Target->GetActorLocation();
	FVector Diff = TargetLoc - OwnerLoc;
	float Dist = Diff.Size();

	if (Dist < Blackboard->GetValueAsFloat(BB_Keys::AttackRadius))
		return true;

	return false;
}
