// Fill out your copyright notice in the Description page of Project Settings.

#include "LoopPathIndex.h"
#include "IEPROJ/Controllers/NPC_AIController.h"
#include "IEPROJ/Characters/NPC.h"
#include "PatrolPath.h"
#include "BlackboardKeys.h"
#include "BehaviorTree/BlackboardComponent.h"

ULoopPathIndex::ULoopPathIndex()
{
	NodeName = TEXT("Increment Path Index");
}

EBTNodeResult::Type ULoopPathIndex::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ANPC_AIController* const Controller = Cast<ANPC_AIController>(OwnerComp.GetAIOwner());
	ANPC* const NPC = Cast<ANPC>(Controller->GetCharacter());
	UBlackboardComponent* const Blackboard = Controller->GetBlackboard();

	APatrolPath* const PatrolPath = NPC->GetPatrolPath();
	const int PointsCount = PatrolPath->Num();
	const int MinIndex = 0;
	const int MaxIndex = PointsCount - 1;
	const int32 CurrentIndex = Blackboard->GetValueAsInt(BB_Keys::PatrolPathIndex);

	if (CurrentIndex >= MaxIndex && Direction == EDirection::Forward)
	{
		Direction = EDirection::Backward;
	}
	else if (CurrentIndex <= MinIndex && Direction == EDirection::Backward)
	{
		Direction = EDirection::Forward;
	}

	if (Direction == EDirection::Forward)
	{
		Blackboard->SetValueAsInt(BB_Keys::PatrolPathIndex, CurrentIndex + 1);
	}
	else
	{
		Blackboard->SetValueAsInt(BB_Keys::PatrolPathIndex, CurrentIndex - 1);
	}

	FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	return EBTNodeResult::Succeeded;
}
