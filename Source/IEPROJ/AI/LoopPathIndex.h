// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "LoopPathIndex.generated.h"

UCLASS()
class IEPROJ_API ULoopPathIndex : public UBTTaskNode
{
	GENERATED_BODY()

public:
	ULoopPathIndex();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
	enum class EDirection
	{
		Forward,
		Backward
	};

	EDirection Direction = EDirection::Forward;
};
