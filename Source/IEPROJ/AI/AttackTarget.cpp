// Fill out your copyright notice in the Description page of Project Settings.

#include "AttackTarget.h"
#include "AIController.h"
#include "IEPROJ/Characters/NPC.h"

UAttackTarget::UAttackTarget()
{
	NodeName = FString("Attack Target");
}

EBTNodeResult::Type UAttackTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (ANPC* const NPC = Cast<ANPC>(OwnerComp.GetAIOwner()->GetCharacter()))
	{
		bool bDidAttackAnyone = NPC->Attack();
		
		if (!bDidAttackAnyone)
		{
			FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
			return EBTNodeResult::Failed;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Owner is not an NPC!"));
		FinishLatentAbort(OwnerComp);
		return EBTNodeResult::Aborted;
	}

	FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	return EBTNodeResult::Succeeded;
}
