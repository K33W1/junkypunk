// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

UCLASS()
class IEPROJ_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMainPlayerController();

	// Game flow
	void ResumeGame();
	void PauseGame();

	// UI
	void OpenMainMenu();
	void CloseMainMenu();

protected:
	virtual void BeginPlay() override;
	
private:
	void InitializeUI();
	
	// UI
	UPROPERTY(EditDefaultsOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UUserWidget> MainMenuWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UUserWidget> FreeRoamPauseMenuWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UUserWidget> LevelPauseMenuWidgetClass;

	UPROPERTY(VisibleInstanceOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	UUserWidget* MainMenuWidget;

	UPROPERTY(VisibleInstanceOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	UUserWidget* FreeRoamPauseMenuWidget;

	UPROPERTY(VisibleInstanceOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	UUserWidget* LevelPauseMenuWidget;

	UUserWidget* CurrentPauseMenuWidget;
	bool bWasCursorVisible = false;
};
