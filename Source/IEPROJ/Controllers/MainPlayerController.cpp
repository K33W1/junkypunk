// Fill out your copyright notice in the Description page of Project Settings.

#include "MainPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "IEPROJ/GameModes/MainGameMode.h"

AMainPlayerController::AMainPlayerController()
{
	
}

void AMainPlayerController::ResumeGame()
{
	CurrentPauseMenuWidget->RemoveFromParent();
	CurrentPauseMenuWidget = nullptr;
	bShowMouseCursor = bWasCursorVisible;
}

void AMainPlayerController::PauseGame()
{
	CurrentPauseMenuWidget = GetWorld()->GetAuthGameMode<AMainGameMode>()->IsInLevel()
		? LevelPauseMenuWidget : FreeRoamPauseMenuWidget;
	CurrentPauseMenuWidget->AddToViewport();
	
	bWasCursorVisible = bShowMouseCursor;
	bShowMouseCursor = true;
}

void AMainPlayerController::OpenMainMenu()
{
	MainMenuWidget->AddToViewport();
	SetInputMode(FInputModeUIOnly());
	bShowMouseCursor = true;
}

void AMainPlayerController::CloseMainMenu()
{
	MainMenuWidget->RemoveFromParent();
}

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	InitializeUI();
	OpenMainMenu();
}

void AMainPlayerController::InitializeUI()
{
	check(MainMenuWidgetClass.Get());
	check(FreeRoamPauseMenuWidgetClass.Get());
	check(LevelPauseMenuWidgetClass.Get());

	// Create all UI
	MainMenuWidget = CreateWidget<UUserWidget>(
		this,
		MainMenuWidgetClass);
	FreeRoamPauseMenuWidget = CreateWidget<UUserWidget>(
		this,
		FreeRoamPauseMenuWidgetClass);
	LevelPauseMenuWidget = CreateWidget<UUserWidget>(
		this,
		LevelPauseMenuWidgetClass);
}