// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "NPC_AIController.generated.h"

struct FAIStimulus;
class UAISenseConfig_Sight;
class UBehaviorTreeComponent;

UCLASS()
class IEPROJ_API ANPC_AIController : public AAIController
{
	GENERATED_BODY()

public:
    ANPC_AIController();

	void SetTarget(AActor* NewTarget);

	UBlackboardComponent* GetBlackboard() const;

protected:
	virtual void BeginPlay() override;
	
	virtual void OnPossess(APawn* InPawn) override;

private:
	UFUNCTION()
	void OnTargetPerceptionUpdated(AActor* Actor, const FAIStimulus Stimulus);

	void SetupPerceptionSystem();
	
	UPROPERTY(EditInstanceOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	UBehaviorTreeComponent* BehaviorTreeComponent;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	UBehaviorTree* BehaviorTree;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	UAIPerceptionComponent* PerceptionComp;

	UAISenseConfig_Sight* SightConfig;
};
