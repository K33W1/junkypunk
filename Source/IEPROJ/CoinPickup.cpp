// Fill out your copyright notice in the Description page of Project Settings.

#include "CoinPickup.h"
#include "BobbingMovementComponent.h"
#include "SpinningMovementComponent.h"
#include "AddSelfToRuntimeDataAssetComponent.h"
#include "MainGameInstance.h"

ACoinPickup::ACoinPickup()
{
	BobbingMovement = CreateDefaultSubobject<UBobbingMovementComponent>(TEXT("BobbingMovementComponent"));
	BobbingMovement->Inject(Mesh);

	SpinningMovement = CreateDefaultSubobject<USpinningMovementComponent>(TEXT("SpinningMovementComponent"));
	SpinningMovement->Inject(Mesh);

	AddSelfToRuntimeDataAsset = CreateDefaultSubobject<UAddSelfToRuntimeDataAssetComponent>(TEXT("UAddSelfToRuntimeDataAssetComponent"));
}

void ACoinPickup::BeginPlay()
{
	Super::BeginPlay();

	// Random direction
	FVector Velocity = FMath::VRand();
	Velocity.Z = FMath::Abs(Velocity.Z); // Only launch self "up"
	Velocity *= FMath::RandRange(150.0f, 200.0f);
	GetRootComponent()->ComponentVelocity = Velocity;
}

void ACoinPickup::OnPlayerOverlapBehavior(APlayerCharacter* _)
{
	if (UMainGameInstance* MainGameInstance =
		Cast<UMainGameInstance>(GetGameInstance()))
	{
		MainGameInstance->Money += Value;
		UE_LOG(LogTemp, Warning, TEXT("Current money: %d"), MainGameInstance->Money);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("GameInstance is not of type MainGameInstance!"));
	}

	Destroy();
}
