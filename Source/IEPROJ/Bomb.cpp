// Fill out your copyright notice in the Description page of Project Settings.

#include "Bomb.h"
#include "Components/SphereComponent.h"
#include "Components/AudioComponent.h"
#include "AddSelfToRuntimeDataAssetComponent.h"

ABomb::ABomb()
{
	PrimaryActorTick.bCanEverTick = false;

	SphereCollider = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollider"));
	SphereCollider->SetSimulatePhysics(true);
	SetRootComponent(SphereCollider);

	BombStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	BombStaticMesh->SetupAttachment(RootComponent);
	
	FuseAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("FuseAudio"));
	FuseAudio->SetupAttachment(RootComponent);

	AddSelfToRuntimeDataAsset = CreateDefaultSubobject<UAddSelfToRuntimeDataAssetComponent>(TEXT("AddSelfToRuntimeDataAsset"));
}

void ABomb::BeginPlay()
{
	Super::BeginPlay();

	SetLifeSpan(Lifespan);
}

void ABomb::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	// Spawn drops
	if (DropsToSpawn.Num() > 0)
	{
		// Spawn random number of drops
		int NumToSpawn = FMath::RandRange(MinDropsToSpawn, MaxDropsToSpawn);
		for (int i = 0; i < NumToSpawn; i++)
		{
			// Get random drop to spawn
			int RandomDropIndex = FMath::RandRange(0, DropsToSpawn.Num() - 1);
			TSubclassOf<AActor> DropToSpawn = DropsToSpawn[RandomDropIndex];

			// Spawn drop
			FActorSpawnParameters SpawnParams;
			float RandomX = FMath::FRandRange(25.0f, 50.0f) * FMath::Pow(-1, static_cast<float>(FMath::RandBool()));
			float RandomY = FMath::FRandRange(25.0f, 50.0f) * FMath::Pow(-1, static_cast<float>(FMath::RandBool()));
			float RandomZ = FMath::FRandRange(50.0f, 75.0f);
			FVector SpawnOffset = FVector(RandomX, RandomY, RandomZ);
			GetWorld()->SpawnActor<AActor>(
				DropToSpawn,
				GetActorLocation() + SpawnOffset,
				FRotator(),
				SpawnParams);
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Bomb does not have any references to any drops!"));
	}
	
	// Spawn explosion
	if (ExplosionClass.Get())
	{
		FActorSpawnParameters SpawnParams;
		GetWorld()->SpawnActor<AActor>(
			ExplosionClass,
			GetActorLocation(),
			FRotator(),
			SpawnParams);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Tried to spawn explosion but null!"));
	}
}
