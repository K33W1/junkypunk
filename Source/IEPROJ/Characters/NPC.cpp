// Fill out your copyright notice in the Description page of Project Settings.

#include "NPC.h"
#include "IEPROJ/AI/BlackboardKeys.h"
#include "IEPROJ/Controllers/NPC_AIController.h"
#include "IEPROJ/DataAssets/Runtime/NPCRuntimeDataAsset.h"
#include "Components/AudioComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

ANPC::ANPC()
{
	PrimaryActorTick.bCanEverTick = true;

	// Movement & rotation
	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;

	// Audio
	AttackAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AttackAudioComponent"));
	AttackAudioComponent->bAutoActivate = false;
}


bool ANPC::Attack()
{
	bool bDidAttackAnyone = false;
	
	TArray<AActor*> ActorsToIgnore;
	TArray<AActor*> ActorsInAttackRange;
	ActorsToIgnore.Add(this);

	UKismetSystemLibrary::SphereOverlapActors
	(
		GetWorld(),
		GetActorLocation(),
		AttackRadius,
		AttackableObjectTypes,
		nullptr,
		ActorsToIgnore,
		ActorsInAttackRange
	);

	for (AActor* ActorToAttack : ActorsInAttackRange)
	{
		ACharacter* CharacterToAttack = Cast<ACharacter>(ActorToAttack);
		
		// Deal damage to other character
		FDamageEvent DamageEvent;
		ActorToAttack->TakeDamage
		(
			static_cast<float>(AttackDamage),
			DamageEvent,
			Controller,
			this
		);

		// Launch other character
		FVector ThisLoc = GetActorLocation();
		FVector OtherLoc = CharacterToAttack->GetActorLocation();
		FVector Diff = OtherLoc - ThisLoc;
		FVector XYDir = FVector::VectorPlaneProject(Diff, FVector(0.0f, 0.0f, 1.0f));
		XYDir.Normalize();
		FVector XYLaunch = XYDir * XYKnockbackVelocity;
		FVector ZLaunch = FVector(0.0f, 0.0f, 1.0f) * ZKnockbackVelocity;
		FVector LaunchVelocity = XYLaunch + ZLaunch;
		CharacterToAttack->LaunchCharacter(LaunchVelocity, true, true);

		// Play hit SFX
		AttackAudioComponent->Play();
		
		bDidAttackAnyone = true;
	}

	return bDidAttackAnyone;
}

void ANPC::ForceDeath()
{
	Destroy();
}

void ANPC::SetPatrolPath(APatrolPath* NewPatrolPath)
{
	PatrolPath = NewPatrolPath;
}

APatrolPath* ANPC::GetPatrolPath() const
{
	return PatrolPath;
}

void ANPC::BeginPlay()
{
	Super::BeginPlay();

	if (EnemyContainer)
	{
		EnemyContainer->NPCs.Add(this);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No container provided to NPC."));
	}

	if (ANPC_AIController* AIController = Cast<ANPC_AIController>(GetController()))
	{
		AIController->GetBlackboard()->SetValueAsFloat(BB_Keys::AttackRadius, AttackRadius);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Controller is not NPC_AIController!"));
	}
}

void ANPC::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

float ANPC::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	ANPC_AIController* AIController = Cast<ANPC_AIController>(GetController());
	AIController->SetTarget(DamageCauser);

	if (PossibleDropsOnHit.Num() > 0)
	{
		// Get random drop
		const int RandomDropIndex = FMath::RandRange(0, PossibleDropsOnHit.Num() - 1);
		const TSubclassOf<AActor> RandomDropClass = PossibleDropsOnHit[RandomDropIndex];

		// Spawn drop
		FActorSpawnParameters SpawnParams;
		GetWorld()->SpawnActor<AActor>(
			RandomDropClass,
			GetActorLocation(),
			FRotator(),
			SpawnParams
		);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("No possible drops on NPC!"));
	}
	
	// Health
	Health -= Damage;
	if (Health <= 0)
	{
		Die();
	}
	
	return Damage;
}

void ANPC::Die()
{
	if (PossibleDropsOnDeath.Num() > 0)
	{
		// Get random drop
		const int RandomDropIndex = FMath::RandRange(0, PossibleDropsOnDeath.Num() - 1);
		const TSubclassOf<AActor> RandomDropClass = PossibleDropsOnDeath[RandomDropIndex];

		// Spawn drop
		FActorSpawnParameters SpawnParams;
		float RandomX = FMath::FRandRange(25.0f, 200.0f) * FMath::Pow(-1, static_cast<float>(FMath::RandBool()));
		float RandomY = FMath::FRandRange(25.0f, 200.0f) * FMath::Pow(-1, static_cast<float>(FMath::RandBool()));
		float RandomZ = FMath::FRandRange(50.0f, 75.0f);
		GetWorld()->SpawnActor<AActor>(
			RandomDropClass,
			GetActorLocation() + FVector(RandomX, RandomY, RandomZ),
			FRotator(),
			SpawnParams);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("No possible drops on NPC!"));
	}

	Destroy();
}

void ANPC::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	
	if (EnemyContainer)
	{
		EnemyContainer->NPCs.Remove(this);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No container provided to NPC."));
	}
}
