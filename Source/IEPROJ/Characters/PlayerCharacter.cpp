// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"
#include "IEPROJ/PlayerTool.h"
#include "IEPROJ/UI/PlayerHealthWidget.h"
#include "IEPROJ/UI/PlayerToolsHotbarWidget.h"
#include "Blueprint/UserWidget.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/AudioComponent.h"

APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Hand
	Hand = CreateDefaultSubobject<USceneComponent>(TEXT("Hand"));
	Hand->SetupAttachment(RootComponent);
	Hand->SetRelativeLocation(FVector(60.0f, 0.0f, 20.0f));

	// Configure character movement
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->bOrientRotationToMovement = true;

	// Camera Boom
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 1000.0f;
	CameraBoom->bUsePawnControlRotation = false;
	CameraBoom->bInheritPitch = false;
	CameraBoom->bInheritYaw = false;
	CameraBoom->bInheritRoll = false;

	// Player Camera
	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));
	PlayerCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	PlayerCamera->bUsePawnControlRotation = false;

	// Move Audio
	MovementAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("MoveAudio"));
	MovementAudio->SetVolumeMultiplier(0.0f);
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	OriginCharacterPos = GetActorLocation();
	OriginCharacterRot = GetActorRotation();
	OriginMeshPos = GetMesh()->GetComponentLocation();
	OriginMeshRot = GetMesh()->GetComponentRotation();

	// Controller
	if (GetPlayerController())
	{
		GetPlayerController()->bShowMouseCursor = true;
		GetPlayerController()->SetInputMode(FInputModeGameAndUI());
	}
}

void APlayerCharacter::Tick(float DeltaSeconds)
{
	if (!GetPlayerController() || Health <= 0)
		return;

	// Adjust view
	int ViewportX = 0;
	int ViewportY = 0;
	GetPlayerController()->GetViewportSize(ViewportX, ViewportY);

	if (ViewportX == 0 || ViewportY == 0)
		return;

	float MouseX = 0.0f;
	float MouseY = 0.0f;
	if (!GetPlayerController()->GetMousePosition(MouseX, MouseY))
		return;

	const FVector2D ViewportSize(ViewportX, ViewportY);
	const FVector2D MouseSize(MouseX, MouseY);
	const FVector2D ViewportMousePos = MouseSize / ViewportSize;
	const FVector2D CenterMousePos = (ViewportMousePos - 0.5f) * 2.0f;
	FVector CameraPos(-CenterMousePos.Y, CenterMousePos.X, 0.0f);
	CameraPos *= CameraRadius;

	PlayerCamera->SetRelativeLocation(FVector::ZeroVector);
	PlayerCamera->SetWorldLocation(PlayerCamera->GetComponentLocation() + CameraPos);

	const float YawRadians = FMath::Atan2(CenterMousePos.X, -CenterMousePos.Y);
	const float Yaw = FMath::RadiansToDegrees(YawRadians);
	const FRotator Rotation = FRotator(0.0f, Yaw, 0.0f);
	SetActorRotation(Rotation);

	// Move SFX
	float CurrentVolume = MovementAudio->VolumeMultiplier;
	float TargetVolume = FMath::Clamp(GetVelocity().SizeSquared() / 1000000.0f, 0.0f, 0.1f);
	float NewVolume = FMath::Lerp(CurrentVolume, TargetVolume, 6.0f * DeltaSeconds);
	MovementAudio->SetVolumeMultiplier(NewVolume);

	// Rotation
	//if (GetVelocity().SizeSquared() >= 0.1f)
	//{
	//	FVector Dir = GetVelocity().GetUnsafeNormal();
	//	FVector Degrees = Dir * GetVelocity().Size() * 0.03f;
	//	FRotator MeshRotation = FRotator(
	//		Degrees.Y,
	//		FRotator::NormalizeAxis(-90.0f),
	//		Degrees.X);
	//	GetMesh()->SetRelativeRotation(MeshRotation);
	//}
}

float APlayerCharacter::TakeDamage(
	float DamageAmount,
	FDamageEvent const& DamageEvent,
	AController* EventInstigator,
	AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (static_cast<int>(DamageAmount) >= 1 && CanBeDamaged())
	{
		Health -= static_cast<int>(DamageAmount);
		HealthWidget->UpdateHearts();
		SetCanBeDamaged(false);

		if (Health > 0)
		{
			FTimerHandle InvulnerableTimerHandle;
			GetWorldTimerManager().SetTimer(
				InvulnerableTimerHandle,
				this,
				&APlayerCharacter::OnInvulnerabilityTimerEnd,
				InvulnerabilityDuration);
		}
		else
		{
			OnDeath();
		}
	}

	return DamageAmount;
}

void APlayerCharacter::Destroyed()
{
	Super::Destroyed();

	for (APlayerTool* Tool : Tools)
	{
		Tool->Destroy();
	}

	if (HealthWidget)
	{
		HealthWidget->RemoveFromParent();
	}
	if (ToolsHotbarWidget)
	{
		ToolsHotbarWidget->RemoveFromParent();
	}
}

void APlayerCharacter::SpawnStartingItems()
{
	for (TSubclassOf<APlayerTool> ToolClass : StartingToolClasses)
	{
		if (ToolClass.Get() == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("ToolClass is null!"));
			continue;
		}

		APlayerTool* NewPlayerTool =
			GetWorld()->SpawnActor<APlayerTool>(ToolClass, GetTransform());

		Tools.Add(NewPlayerTool);
		USceneComponent* ToolRoot = NewPlayerTool->GetRootComponent();
		FAttachmentTransformRules AttachmentTransformRules = FAttachmentTransformRules(
			EAttachmentRule::SnapToTarget,
			EAttachmentRule::SnapToTarget,
			EAttachmentRule::SnapToTarget,
			true);
		ToolRoot->AttachToComponent(Hand, AttachmentTransformRules);
		NewPlayerTool->OnAttach(this);
	}
}

void APlayerCharacter::CreateHeartsHUD(AController* NewController)
{
	APlayerController* PlayerController = Cast<APlayerController>(NewController);

	if (!PlayerController)
	{
		UE_LOG(LogTemp, Warning, TEXT("No PlayerController to PlayerCharacter attached!"));
		return;
	}

	if (HealthWidgetClass.Get())
	{
		HealthWidget = CreateWidget<UPlayerHealthWidget>(
			PlayerController,
			HealthWidgetClass);
		HealthWidget->Inject(this);
		HealthWidget->AddToViewport();
		HealthWidget->UpdateHearts();
	}
	else if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(
			-1,
			15.0f,
			FColor::Yellow,
			TEXT("No HealthHUD Class Referenced!"));
	}
}

void APlayerCharacter::CreateToolsHUD(AController* NewController)
{
	APlayerController* PlayerController = Cast<APlayerController>(NewController);

	if (!PlayerController)
	{
		UE_LOG(LogTemp, Warning, TEXT("No PlayerController to PlayerCharacter attached!"));
		return;
	}

	if (ToolsHotbarWidgetClass.Get())
	{
		ToolsHotbarWidget = CreateWidget<UPlayerToolsHotbarWidget>(
			PlayerController,
			ToolsHotbarWidgetClass);
		ToolsHotbarWidget->SetPlayerCharacter(this);
		ToolsHotbarWidget->AddToViewport();
	}
	else if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(
			-1,
			15.0f,
			FColor::Yellow,
			TEXT("No PlayerToolsHUD Class Referenced!"));
	}
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &APlayerCharacter::Action);
	PlayerInputComponent->BindAction("Equip1", IE_Pressed, this, &APlayerCharacter::EquipTool1);
	PlayerInputComponent->BindAction("Equip2", IE_Pressed, this, &APlayerCharacter::EquipTool2);
	PlayerInputComponent->BindAction("Equip3", IE_Pressed, this, &APlayerCharacter::EquipTool3);
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
}

void APlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	SpawnStartingItems();
	CreateHeartsHUD(NewController);
	CreateToolsHUD(NewController);

	OriginControllerRot = NewController->GetControlRotation();

	if (!EquippedTool)
	{
		EquipTool(0);
	}
}

void APlayerCharacter::ForceDeath()
{
	SetCanBeDamaged(true);
	TakeDamage(
		Health,
		FDamageEvent(),
		GetController(),
		this);
}

void APlayerCharacter::StartGame()
{
	// Health
	Health = MaxHealth;
	HealthWidget->UpdateHearts();
	SetCanBeDamaged(true);

	for (APlayerTool* Tool : Tools)
	{
		Tool->Restart();
	}

	GetCharacterMovement()->SetMovementMode(MOVE_Walking);
	GetPlayerController()->EnableInput(GetPlayerController());
	GetPlayerController()->SetInputMode(FInputModeGameAndUI());
	GetPlayerController()->bShowMouseCursor = true;
}

int APlayerCharacter::GetMaxHealth() const
{
	return MaxHealth;
}

int APlayerCharacter::GetHealth() const
{
	return Health;
}

TArray<APlayerTool*> APlayerCharacter::GetPlayerTools() const
{
	return Tools;
}

APlayerController* APlayerCharacter::GetPlayerController() const
{
	return Cast<APlayerController>(GetController());
}

void APlayerCharacter::MoveForward(float Value)
{
	if ((GetController() != nullptr) && (Value != 0.0f))
	{
		const FRotator Rotation = GetController()->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		AddMovementInput(Direction, Value);
	}
}

void APlayerCharacter::MoveRight(float Value)
{
	if ((GetController() != nullptr) && (Value != 0.0f))
	{
		const FRotator Rotation = GetController()->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		AddMovementInput(Direction, Value);
	}
}

void APlayerCharacter::Action()
{
	if (EquippedTool)
	{
		EquippedTool->Use();
	}
}

void APlayerCharacter::EquipTool1()
{
	EquipTool(0);
}

void APlayerCharacter::EquipTool2()
{
	EquipTool(1);
}

void APlayerCharacter::EquipTool3()
{
	EquipTool(2);
}

void APlayerCharacter::EquipTool(int Index)
{
	if (EquippedTool)
	{
		EquippedTool->OnUnequip();
	}

	if (Index < Tools.Num())
	{
		EquippedTool = Tools[Index];
		EquippedTool->OnEquip();
	}
}

void APlayerCharacter::OnDeath()
{
	GetMesh()->SetSimulatePhysics(true);
	GetCharacterMovement()->DisableMovement();

	for (APlayerTool* Tool : Tools)
	{
		Tool->SetActorHiddenInGame(true);
	}

	OnPlayerDeath.Broadcast();

	GetPlayerController()->DisableInput(GetPlayerController());
	GetPlayerController()->bShowMouseCursor = true;
}

void APlayerCharacter::OnInvulnerabilityTimerEnd()
{
	SetCanBeDamaged(true);
}
