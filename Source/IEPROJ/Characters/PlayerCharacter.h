// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class APlayerTool;
class UPlayerHealthWidget;
class UPlayerToolsHotbarWidget;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerDeath);

UCLASS(Abstract, Blueprintable)
class IEPROJ_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	APlayerCharacter();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PossessedBy(AController* NewController) override;

	// Call in editor
	UFUNCTION(CallInEditor, Category = "Debug")
	void ForceDeath();

	// Game flow
	void StartGame();

	// Getters
	int GetMaxHealth() const;
	int GetHealth() const;
	TArray<APlayerTool*> GetPlayerTools() const;
	APlayerController* GetPlayerController() const;

	// Delegates
	UPROPERTY(BlueprintAssignable)
	FOnPlayerDeath OnPlayerDeath;
	
protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;
	
	virtual float TakeDamage(
		float DamageAmount,
		struct FDamageEvent const& DamageEvent,
		class AController* EventInstigator,
		AActor* DamageCauser) override;

	virtual void Destroyed() override;
	
private:
	// BeginPlay helpers
	void SpawnStartingItems();
	void CreateHeartsHUD(AController* NewController);
	void CreateToolsHUD(AController* NewController);

	// Input movement
	void MoveForward(float Value);
	void MoveRight(float Value);

	// Input actions
	void Action();
	void EquipTool1();
	void EquipTool2();
	void EquipTool3();
	void EquipTool(int Index);

	// Invulnerability
	void OnInvulnerabilityTimerEnd();

	// Player death
	void OnDeath();
	void StartRagdoll();

	// Components
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* Hand;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* PlayerCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAudioComponent* MovementAudio;

	// References
	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UPlayerHealthWidget> HealthWidgetClass;

	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UPlayerToolsHotbarWidget> ToolsHotbarWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Interaction, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<APlayerTool>> StartingToolClasses;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Interaction, meta = (AllowPrivateAccess = "true"))
	APlayerTool* EquippedTool;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Interaction, meta = (AllowPrivateAccess = "true"))
	TArray<APlayerTool*> Tools;

	// Settings
	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	int MaxHealth = 3;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	int Health = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float MoveAcceleration = 50.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float InvulnerabilityDuration = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float CameraRadius = 100.0f;

	// UI
	UPROPERTY(VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
	UPlayerHealthWidget* HealthWidget;
	
	UPROPERTY(VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
	UPlayerToolsHotbarWidget* ToolsHotbarWidget;

	// Restarting
	FVector OriginCharacterPos;
	FVector OriginMeshPos;
	FRotator OriginCharacterRot;
	FRotator OriginControllerRot;
	FRotator OriginMeshRot;
};
