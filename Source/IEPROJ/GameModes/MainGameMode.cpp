// Fill out your copyright notice in the Description page of Project Settings.

#include "MainGameMode.h"
#include "IEPROJ/Managers/LevelManager.h"
#include "IEPROJ/Managers/FreeRoamManager.h"
#include "IEPROJ/Controllers/MainPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "Camera/CameraActor.h"
#include "IEPROJ/DataAssets/Runtime/CameraActorRuntimeDataAsset.h"

AMainGameMode::AMainGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
	
	DefaultPawnClass = APawn::StaticClass();
}

void AMainGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	InitializePlayer();
	InitializeLevels();
	InitializeFreeRoamManager();
}

void AMainGameMode::InitializePlayer()
{
	PlayerController = GetWorld()->GetFirstPlayerController<AMainPlayerController>();
	check(PlayerController);
}

void AMainGameMode::InitializeLevels()
{
	// Find all levels
	TArray<AActor*> LevelActors;
	UGameplayStatics::GetAllActorsOfClass(
		GetWorld(),
		ALevelManager::StaticClass(),
		LevelActors);

	// Initialize all LevelManagers
	for (AActor* LevelActor : LevelActors)
	{
		ALevelManager* Level = Cast<ALevelManager>(LevelActor);
		Levels.Add(Level);
	}
}

void AMainGameMode::InitializeFreeRoamManager()
{
	FreeRoamManager = Cast<AFreeRoamManager>(
		UGameplayStatics::GetActorOfClass(
			GetWorld(),
			AFreeRoamManager::StaticClass()));
	check(FreeRoamManager);
}

void AMainGameMode::StartLevel(int Index)
{
	CurrentLevelManager = Levels[Index];
	CurrentLevelManager->EnterLevel();
	bIsInLevel = true;
}

void AMainGameMode::RestartLevel()
{
	check(CurrentLevelManager);
	CurrentLevelManager->RestartGame();
}

void AMainGameMode::ExitLevel()
{
	check(CurrentLevelManager);
	bIsInLevel = false;
	CurrentLevelManager->ExitLevel();
}

void AMainGameMode::TogglePause()
{
	const bool bIsGamePaused = GetWorld()->IsPaused();
	
	if (bIsGamePaused)
	{
		UGameplayStatics::SetGamePaused(GetWorld(), false);
		PlayerController->ResumeGame();
	}
	else
	{
		UGameplayStatics::SetGamePaused(GetWorld(), true);
		PlayerController->PauseGame();
	}
}

ALevelManager* AMainGameMode::GetCurrentLevelManager() const
{
	check(CurrentLevelManager);
	return CurrentLevelManager;
}

AFreeRoamManager* AMainGameMode::GetFreeRoamManager() const
{
	check(FreeRoamManager);
	return FreeRoamManager;
}

void AMainGameMode::OpenMainMenu()
{
	PlayerController->OpenMainMenu();
}

void AMainGameMode::SwitchToMainMenuCamera()
{
	const TArray<ACameraActor*>& Cameras = MainMenuCameras->Cameras;
	ACameraActor* RandomCamera = Cameras[FMath::Rand() % Cameras.Num()];
	
	PlayerController->SetViewTargetWithBlend(
		RandomCamera,
		BlendToMainMenuCameraTime);
}

void AMainGameMode::CloseMainMenu()
{
	PlayerController->CloseMainMenu();
}

bool AMainGameMode::IsInLevel() const
{
	return bIsInLevel;
}
