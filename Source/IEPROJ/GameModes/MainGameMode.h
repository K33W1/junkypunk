// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainGameMode.generated.h"

class AMainPlayerController;
class ALevelManager;
class AFreeRoamManager;
class APlayerCharacter;
class UCameraActorRuntimeDataAsset;

UCLASS()
class IEPROJ_API AMainGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMainGameMode();
	
	UFUNCTION(BlueprintCallable)
	void OpenMainMenu();

	UFUNCTION(BlueprintCallable)
	void SwitchToMainMenuCamera();

	UFUNCTION(BlueprintCallable)
	void CloseMainMenu();
	
	UFUNCTION(BlueprintCallable)
	void StartLevel(int Index);

	UFUNCTION(BlueprintCallable)
	void RestartLevel();

	UFUNCTION(BlueprintCallable)
	void ExitLevel();

	UFUNCTION(BlueprintCallable)
	void TogglePause();

	UFUNCTION(BlueprintCallable)
	ALevelManager* GetCurrentLevelManager() const;
	
	UFUNCTION(BlueprintCallable)
	AFreeRoamManager* GetFreeRoamManager() const;

	bool IsInLevel() const;

protected:
	virtual void BeginPlay() override;

private:
	void InitializePlayer();
	void InitializeLevels();
	void InitializeFreeRoamManager();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraActorRuntimeDataAsset* MainMenuCameras;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float BlendToMainMenuCameraTime = 2.0f;
	
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<ALevelManager*> Levels;

	// Managers
	ALevelManager* CurrentLevelManager;
	AFreeRoamManager* FreeRoamManager;
	bool bIsInLevel;

	// Player
	AMainPlayerController* PlayerController;
	APlayerCharacter* PlayerCharacter;
};
