// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Explosion.h"
#include "GameFramework/Actor.h"
#include "Bomb.generated.h"

class UAddSelfToRuntimeDataAssetComponent;
class UActorRuntimeDataAsset;
class USphereComponent;

UCLASS(Abstract)
class IEPROJ_API ABomb : public AActor
{
	GENERATED_BODY()
	
public:	
	ABomb();
	
protected:
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
private:
	// Components
    UPROPERTY(VisibleInstanceOnly, meta = (AllowPrivateAccess = "true"))
    USphereComponent* SphereCollider;

    UPROPERTY(VisibleInstanceOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BombStaticMesh;

    UPROPERTY(VisibleInstanceOnly, meta = (AllowPrivateAccess = "true"))
	UAudioComponent* FuseAudio;

	UPROPERTY(VisibleInstanceOnly, meta = (AllowPrivateAccess = "true"))
	UAddSelfToRuntimeDataAssetComponent* AddSelfToRuntimeDataAsset;

    // References
	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AExplosion> ExplosionClass;

	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<AActor>> DropsToSpawn;

	// Settings
	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	float Lifespan = 3.0f;

	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	int MinDropsToSpawn = 1;

	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	int MaxDropsToSpawn = 3;
};
