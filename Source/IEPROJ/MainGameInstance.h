// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MainGameInstance.generated.h"

class UMainSaveGame;
class UToolUpgrades;

UCLASS()
class IEPROJ_API UMainGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	virtual void Init() override;
	
	virtual void Shutdown() override;

	UFUNCTION(BlueprintCallable)
	void SaveGame();
	
	UFUNCTION(BlueprintCallable)
	void LoadGame();

	UFUNCTION(BlueprintCallable)
	void ClearSavedGame();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Money;

private:
	void InitializePlayerTools();
	
	const FString MainSaveSlot = FString("SaveSlot");

	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess))
	TArray<UToolUpgrades*> PlayerToolUpgrades;
};
