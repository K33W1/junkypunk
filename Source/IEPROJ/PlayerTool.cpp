// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerTool.h"
#include "UI/PlayerToolWidget.h"
#include "IEPROJ/Characters/PlayerCharacter.h"

APlayerTool::APlayerTool()
{
	PrimaryActorTick.bCanEverTick = false;

	PlayerToolWidgetClass = UPlayerToolWidget::StaticClass();
}

void APlayerTool::Initialize()
{
	if (PlayerToolWidgetClass)
	{
		APlayerController* PlayerController = PlayerCharacter->GetPlayerController();
		PlayerToolWidget = CreateWidget<UPlayerToolWidget>(
			PlayerController,
			PlayerToolWidgetClass);
	}
	else if (MissingPlayerToolWidgetClass)
	{
		APlayerController* PlayerController = PlayerCharacter->GetPlayerController();
		PlayerToolWidget = CreateWidget<UPlayerToolWidget>(
			PlayerController,
			MissingPlayerToolWidgetClass);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No widget to create for player tool!"));
		return;
	}

	PlayerToolWidget->Inject(this);
	PlayerToolWidget->AddToViewport();
}

void APlayerTool::Use_Implementation()
{
	
}

void APlayerTool::OnAttach_Implementation(APlayerCharacter* NewPlayerCharacter)
{
	PlayerCharacter = NewPlayerCharacter;
}

void APlayerTool::OnUnattach_Implementation()
{
	PlayerCharacter = nullptr;
}

void APlayerTool::OnEquip_Implementation()
{
	GetRootComponent()->SetVisibility(true, true);
	PlayerToolWidget->OnEquip();
}

void APlayerTool::OnUnequip_Implementation()
{
	GetRootComponent()->SetVisibility(false, true);
	PlayerToolWidget->OnUnequip();
}

void APlayerTool::Restart_Implementation()
{
	SetActorHiddenInGame(false);
}

UPlayerToolWidget* APlayerTool::GetPlayerToolWidget()
{
	return PlayerToolWidget;
}

void APlayerTool::BeginPlay()
{
	Super::BeginPlay();
	
	GetRootComponent()->SetVisibility(false, true);
}
