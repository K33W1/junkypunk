// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerTool.generated.h"

class APlayerCharacter;
class UPlayerToolWidget;

UCLASS(abstract, Blueprintable, BlueprintType)
class IEPROJ_API APlayerTool : public AActor
{
	GENERATED_BODY()
	
public:	
	APlayerTool();

	void Initialize();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Use();
	void Use_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnAttach(APlayerCharacter* NewPlayerCharacter);
	void OnAttach_Implementation(APlayerCharacter* NewPlayerCharacter);
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnUnattach();
	void OnUnattach_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnEquip();
	void OnEquip_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnUnequip();
	void OnUnequip_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Restart();
	void Restart_Implementation();

	UPlayerToolWidget* GetPlayerToolWidget();

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UPlayerToolWidget> PlayerToolWidgetClass;
	
	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UPlayerToolWidget> MissingPlayerToolWidgetClass;

	UPROPERTY(EditInstanceOnly, meta = (AllowPrivateAccess = "true"))
	UPlayerToolWidget* PlayerToolWidget;

	APlayerCharacter* PlayerCharacter;
};
