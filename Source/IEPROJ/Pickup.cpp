// Fill out your copyright notice in the Description page of Project Settings.

#include "Pickup.h"
#include "IEPROJ/Characters/PlayerCharacter.h"
#include "Components/SphereComponent.h"

APickup::APickup()
{
	PrimaryActorTick.bCanEverTick = false;

	Collider = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollider"));
	Collider->SetSphereRadius(64.0f);
	Collider->BodyInstance.bLockXRotation = true;
	Collider->BodyInstance.bLockYRotation = true;
	Collider->BodyInstance.bLockZRotation = true;
	Collider->SetCollisionProfileName(TEXT("ItemCollider"));
	Collider->SetSimulatePhysics(true);
	SetRootComponent(Collider);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCollisionProfileName(TEXT("NoCollision"));

	Trigger = CreateDefaultSubobject<USphereComponent>(TEXT("SphereTrigger"));
	Trigger->SetupAttachment(RootComponent);
	Trigger->SetSphereRadius(128.0f);
	Trigger->SetCollisionProfileName(TEXT("ItemTrigger"));
	Trigger->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnPlayerOverlap);
}

void APickup::OnPlayerOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor))
	{
		OnPlayerOverlapBehavior(PlayerCharacter);
	}
}

void APickup::OnPlayerOverlapBehavior(APlayerCharacter* PlayerCharacter)
{
	UE_LOG(LogTemp, Warning, TEXT("No Player Overlap Behaviour Implemented!"));
}
