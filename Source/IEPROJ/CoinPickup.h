// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "CoinPickup.generated.h"

class UBobbingMovementComponent;
class USpinningMovementComponent;
class UAddSelfToRuntimeDataAssetComponent;

UCLASS()
class IEPROJ_API ACoinPickup : public APickup
{
	GENERATED_BODY()

public:
	ACoinPickup();
	
protected:
	virtual void BeginPlay() override;
	
	virtual void OnPlayerOverlapBehavior(APlayerCharacter* PlayerCharacter) override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UBobbingMovementComponent* BobbingMovement;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USpinningMovementComponent* SpinningMovement;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAddSelfToRuntimeDataAssetComponent* AddSelfToRuntimeDataAsset;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	int Value = 1;
};
