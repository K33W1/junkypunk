// Fill out your copyright notice in the Description page of Project Settings.

#include "AddSelfToRuntimeDataAssetComponent.h"
#include "IEPROJ/DataAssets/Runtime/ActorRuntimeDataAsset.h"

UAddSelfToRuntimeDataAssetComponent::UAddSelfToRuntimeDataAssetComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UAddSelfToRuntimeDataAssetComponent::BeginPlay()
{
	Super::BeginPlay();

	if (ActorRuntimeDataAsset)
	{
		ActorRuntimeDataAsset->Add(GetOwner());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Actor Runtime Data Asset of AddSelfToRuntimeDataAssetComponent is null!"));
	}
}

void UAddSelfToRuntimeDataAssetComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	
	if (ActorRuntimeDataAsset)
	{
		ActorRuntimeDataAsset->Remove(GetOwner());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Actor Runtime Data Asset of AddSelfToRuntimeDataAssetComponent is null!"));
	}
}
